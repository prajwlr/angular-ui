import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";


@Component({
  selector: 'app-advice-service',
  templateUrl: './advice-service.component.html',
  styleUrls: ['./advice-service.component.scss']
})
export class AdviceServiceComponent implements OnInit {


  stockTicker: any;
  advice: any = [];
  
  constructor(public restApi: RestApiService,
    private route: ActivatedRoute) { }


    
  ngOnInit(): void {
    this.stockTicker = this.route.snapshot.params["stockTicker"];

    this.restApi.advice_service(this.stockTicker).subscribe((data: {}) => {
      this.advice = data;
  });
  }
  

}
