import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdviceServiceComponent } from './advice-service.component';

describe('AdviceServiceComponent', () => {
  let component: AdviceServiceComponent;
  let fixture: ComponentFixture<AdviceServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdviceServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdviceServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
