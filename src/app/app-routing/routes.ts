import { Routes } from '@angular/router';

import { HeaderComponent } from '../header/header.component';
import { LoginComponent } from '../login/login.component';
import { HomeComponent} from '../home/home.component';
import { DashboardfComponent } from '../dashboardf/dashboardf.component';
import { CartComponent } from '../cart/cart.component';
import { LogoutComponent } from '../logout/logout.component';
import { StockdetailsComponent} from '../stockdetails/stockdetails.component';
import {PortfolioComponent} from '../portfolio/portfolio.component';
import { AdviceServiceComponent } from '../advice-service/advice-service.component';

export const routes: Routes = [
  { path: 'home',  component: HomeComponent },
  { path: 'login',  component: LoginComponent },
  { path: 'dashboardf',  component: DashboardfComponent },
  { path: 'cart',  component: CartComponent },
  { path: 'logout',   component:LogoutComponent},
  { path: 'portfolio', component: PortfolioComponent},
  { path: 'stock-status/:id', component: StockdetailsComponent },
  { path: 'advice-service/:stockTicker', component: AdviceServiceComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];