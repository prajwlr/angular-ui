import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ActivatedRoute} from '@angular/router';
import { Stocks } from '../shared/stocks';
import {CartComponent} from '../cart/cart.component';

@Component({
  selector: 'app-stockdetails',
  templateUrl: './stockdetails.component.html',
  styleUrls: ['./stockdetails.component.scss']
})
export class StockdetailsComponent implements OnInit {

  id: any;

  Stocks: any = [];
  constructor(public restApi: RestApiService,
    private route: ActivatedRoute) { }

    getMessage(){

      let code: number = this.Stocks.statusCode;
      if (code == 0){
        this.Stocks.statusCode = "Order Placed";
      }
      else if (code == 1){
        this.Stocks.statusCode = "Processing";
      }
      else if (code = 2){
        this.Stocks.statusCode = "Success";
      }
      else if ( code == 3){
        this.Stocks.statusCode = "Stock Not Found";
      }

      else if ( code == 4){
        this.Stocks.statusCode = "Sell Limit Exceeded";
      }
    }
  

    
  ngOnInit(): void {
    this.id = this.route.snapshot.params["id"];

    this.restApi.getStocksById(this.id).subscribe((data: {}) => {
      this.Stocks = data;
      this.getMessage();

  });


}
}
