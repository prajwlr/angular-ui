import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ActivatedRoute} from '@angular/router';
import { PortfolioItem } from '../shared/portfolio-item';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {





  PortfolioItem: any = [];
  constructor(public restApi: RestApiService,
    private route: ActivatedRoute) { }


    
  ngOnInit(): void {
this.loadPortfolio();
}

loadPortfolio() {
  return this.restApi.getAllPortfolioItems().subscribe((data: {}) => {
      this.PortfolioItem = data;
  })
}
}