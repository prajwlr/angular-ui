
import { Component,Input, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";
import { Stocks } from '../shared/stocks';
import { ActivatedRoute} from '@angular/router';
import { AdviceServiceComponent} from '../advice-service/advice-service.component';

interface Animal {
  name: string;
  sound: string;
}


@Component({
  selector: 'app-dashboardf',
  templateUrl: './dashboardf.component.html',
  styleUrls: ['./dashboardf.component.scss']
})


export class DashboardfComponent implements OnInit {
  name = 'Angular 5';
  advice :any =[]

  Stocks: any = [];

  selectFormControl = new FormControl('', Validators.required);

  @Input() stockdetails = { id:0, datetime:"",stockTicker: "", price: 0 , volume: 0, buyOrSell:"",statusCode:0 }

  constructor(
    private route: ActivatedRoute,
    public restApi: RestApiService, 
    public router: Router
  ) { }

  ngOnInit(): void {
    this.loadStocks()
  }

  loadStocks() {
    return this.restApi.getAllStocks().subscribe((data: {}) => {
        this.Stocks = data;
    })
  }

  getStockById(id: number){
    this.router.navigate(['stock-status',id])
  }


  addStocks() {
    this.restApi.addStocks(this.stockdetails).subscribe((data: {}) => {
      this.router.navigate(['/cart'])
    })
  }
  getPrice(){
    this.restApi.advice_service(this.stockdetails.stockTicker).subscribe((data: {}) => {
      this.advice = data;
      this.stockdetails.price = this.advice.lastClose;

  })
}
}

