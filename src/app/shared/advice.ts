export class Advice {
    constructor(){
        
        this.lastClose = 0;
        this.upperBand = 0;
        this.lowerBand =0;
        this.advice ="";

    }

    lastClose :number;
    upperBand :number;
    lowerBand :number;
    advice :String;

}