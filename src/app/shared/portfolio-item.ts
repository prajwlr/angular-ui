export class PortfolioItem {
    constructor(){
        
		this.stockTicker = "";
		this.price = 0;
		this.volume = 0;
		this.buyOrSell = "";
		this.statusCode = 0;
		this.datetime = "";
	}
    
    stockTicker: String;
    price:number;
    volume:number;
    buyOrSell:String;
    statusCode:number;
    datetime:String;

}
