import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Stocks } from '../shared/stocks';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import {Advice} from '../shared/advice';
import { PortfolioItem } from './portfolio-item';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  apiURL = 'http://localhost:8080/api/Stocks/';
  apiAdviceURL = "http://localhost:5000/v1/advice?ticker=";
  portapiURL ="http://localhost:8090/api/Portfolio/";
  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  advice_service(stockTicker : String): Observable<Advice> {
    return this.http.get<Advice>(this.apiAdviceURL+stockTicker)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  // HttpClient API get() method 
  getAllStocks(): Observable<Stocks> {
    return this.http.get<Stocks>(this.apiURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getAllPortfolioItems(): Observable<PortfolioItem> {
    return this.http.get<PortfolioItem>(this.portapiURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API post() method 
  addStocks(stocks:Stocks): Observable<Stocks> {
    return this.http.post<Stocks>(this.apiURL + '', JSON.stringify(stocks), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )

    }
   
    // HttpClient API get() method 
  getStocksById(id : number): Observable<Stocks> {
    return this.http.get<Stocks>(this.apiURL+id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  
    
    
  // Error handling 
  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
}
